# Sleeper Survival Android Game

This is my first attempt at creating a mobile game for Android using the Unity game engine. 
The assets used in this game are part of a now-deprecated tutorial series 
that used to be availabe on Unity. Although the assets are no longer available, the 
tutorial series is still available here: [Survival Shooter Tutorial](https://learn.unity.com/project/survival-shooter-tutorial).

This game was built using an older version of Unity (2017.2.0), but build scripts 
can by upgraded to newer/current versions of Unity*. 

*This may require updating library calls/imports within the C# scripts.
